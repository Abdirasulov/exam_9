﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
    public class Transaction
    {
        [Key]
        public int Id { get; set; }
        public int Summ { get; set; }


        public User UserFrom { get; set; }
        public User UserTo { get; set; }
        [ForeignKey("UserFrom")]
        public string FromUserId { get; set; }
        [ForeignKey("UserTo")]
        public string ToUserId { get; set; }
        
        public DateTime ActionDate { get; set; }
    }
}
