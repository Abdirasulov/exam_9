﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;



using System;


namespace MoneyTransfer.Models
{
    public class ApplicationContext : IdentityDbContext<User>
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

        public ApplicationContext()
        {
        }

        public DbSet<Transaction> Transactions { get; set; }
        public class ApplicationUser : IdentityUser
        {
            public string Name { get; set; }
            public int Balance { get; set; }

        }
        public static ApplicationContext Create()
        {
            return new ApplicationContext();
        }


    }



}