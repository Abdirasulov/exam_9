﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Controllers
{
    public class TransactionController : Controller
    {
        private Entities db = new Entities();

        // GET: Transactions

        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly ApplicationContext context;
        public TransactionController(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            this.context = context;
        }

        [HttpGet]
        public IActionResult Transfer()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Transfer(TransferViewModel model)
        {
            User user = context.Users.FirstOrDefault(u => u.UniqueNumber == model.UniqueNumber);
            User currentUser = await _userManager.FindByNameAsync(User.Identity.Name);
            Transaction transaction = new Transaction()
            {
                ActionDate = DateTime.Now,
                Summ = model.Balance,
                FromUserId = currentUser.UserName,
                ToUserId = user.UserName

            }
            ;

            if (currentUser.Balance >= model.Balance)
            {
                user.Balance += model.Balance;

                currentUser.Balance -= model.Balance;
                context.Transactions.Add(transaction);
                context.SaveChanges();
                return View("Transaction", "Transaction");
            }
            return View("NoCash");
        }


        //public async Task<IActionResult> Transaction(TransactionViewModel model)
        //{
        //    User currentUser = await _userManager.FindByNameAsync(User.Identity.Name);
        //    List<Transaction> transactions = context.Transactions
        //        .Where(t => t.FromUserId == currentUser.UniqueNumber ||
        //                    t.ToUserId == currentUser.UniqueNumber).
        //        OrderByDescending(t => t.ActionDate).ToList();
        //    return View(transactions);
        //}
        [Authorize]
        public async Task<IActionResult> Transaction(TransactionViewModel model)
        {
            User CurrenUser = await _userManager.FindByNameAsync(User.Identity.Name);
            List<Transaction> transactions = context.Transactions.Include(u => u.UserFrom).Include(u => u.UserTo)
                .Where(t => t.FromUserId == CurrenUser.UniqueNumber || t.ToUserId == CurrenUser.UniqueNumber).ToList();
            return View(transactions);

        }
        //[HttpPost]
        //public IActionResult Add(TransferViewModel model)
        //{
        //    User user = context.Users.FirstOrDefault(u => u.UniqueNumber == model.UniqueNumber);
        //    user.Balance += model.Balance;
        //    Transaction transaction = new Transaction()
        //    {
        //        ActionDate = DateTime.Now,
        //        Summ = model.Balance,

        //        ToUserId = user.Id,

        //    };
        //    context.Transactions.Add(transaction);
        //    context.SaveChanges();
        //    return View("Index");
        //}
        //public async Task<IActionResult> Transaction(TransactionViewModel model)
        //{
        //    User CurrentUser = await _userManager.FindByEmailAsync(User.Identity.Name);
        //    List<Transaction> transactions = context.Transactions.Include(u => u.UserTo).Include(u => u.UserTo)
        //        .Where(t => t.FromUserId == CurrentUser.UniqueNumber || t.ToUserId == CurrentUser.UniqueNumber).ToList();
        //    return View(transactions);
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit( Transaction transactions)
        //{
        //    if (transactions == null)
        //    {
        //        throw new ArgumentNullException(nameof(transactions));
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(transactions).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Transaction");
        //    }
        //    return View(transactions);
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create(TransactionViewModel mod, Transaction transactions)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View("Create", transactions);
        //    }
        //    using (var context = new ApplicationContext())
        //    {
        //        transactions.ActionDate = DateTime.Now; //initializing datetime

        //        var store = new UserStore<User>(context);
        //        var manager = new UserManager<User>(store);

        //        var currentUser = manager.FindByIdAsync(User.Identity.Name()); //getting current user (sender) by id           
        //        var queryresult = from u in db.AspNetUsers  //getting correspondent user with selected name
        //                          where u.Name == transactions.CorrespondentName
        //                          select u;

        //        var correspondentUser = manager.FindById(queryresult.First().Id.ToString()); //finally getting correspondent user by id
        //        int resultBalance = currentUser.Balance - transactions.TransactionAmount;
        //        transactions.UserId = currentUser.Id;

        //        if (resultBalance >= 0) //else show message
        //        {
        //            if (ModelState.IsValid && (correspondentUser != null))
        //            {
        //                transactions.ResultBalance = resultBalance; //initializing result balance
        //                db.Transactions.Add(transactions);
        //                currentUser.Balance = resultBalance;
        //                correspondentUser.Balance += transactions.TransactionAmount; //transfer money

        //                var result1 = manager.Update(currentUser);
        //                context.Entry(currentUser).State = EntityState.Modified;
        //                var result2 = manager.Update(correspondentUser);
        //                context.Entry(correspondentUser).State = EntityState.Modified;
        //                if (!result1.Succeeded || !result2.Succeeded)
        //                {
        //                    Response.Write("<script>alert('Sorry, transaction can't be completed. Try again later.')</script>");
        //                    return View("Create", transactions);
        //                }
        //                else
        //                {
        //                    db.SaveChanges(); //saving transaction
        //                    var ctx = store.Context;
        //                    ctx.SaveChanges(); //updating users' balances
        //                    Response.Write("<script>alert('Transaction completed succesfully!')</script>");
        //                }
        //                return RedirectToAction("Index");
        //            }
        //        }
        //        else Response.Write("<script>alert('You do not have enough money for this transaction.')</script>");

        //        return View(transactions);
        //    }
        //}
    }
}
