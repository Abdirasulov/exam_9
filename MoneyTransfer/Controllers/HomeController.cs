﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoneyTransfer.Models;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationContext context;

        public HomeController(ApplicationContext context)
        {
            this.context = context;
        }
     
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Add(TransferViewModel model)
        {
            User user = context.Users.FirstOrDefault(u => u.UniqueNumber == model.UniqueNumber);
            user.Balance += model.Balance;
            Transaction transaction = new Transaction()
            {
                ActionDate = DateTime.Now,
                Summ = model.Balance,

                ToUserId = user.Id,

            };
            context.Transactions.Add(transaction);
            context.SaveChanges();
            return View("Index");
        }
    }
}
