﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class TransactionViewModel
    {
        public DateTime ActionDate { get; set; }
        [Required]
        [Range(1, 1000000, ErrorMessage = "Сумма перевода не может быть меньше 1 и больше 1000000.")]
        public int Summ { get; set; }
        public string FromUserId { get; set; }
        public string ToUserId { get; set; }
    }
}
