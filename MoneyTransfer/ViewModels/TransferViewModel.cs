﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class TransferViewModel
    {
        [Required]
        [Range(1, 1000000, ErrorMessage = "Сумма перевода не может быть меньше 1 и больше 1000000.")]
        public int Balance { get; set; }
        public string UniqueNumber { get; set; }
    }
}
